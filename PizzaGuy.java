import java.util.Queue;
import java.util.concurrent.Semaphore;

public class PizzaGuy implements Runnable {

	private final Semaphore mutex;

	private final Queue<Integer> pizza;

	private final Semaphore pizzaEmpty;

	private final Semaphore pizzaFull;

	private final Semaphore pizzaRequest;

	public PizzaGuy(
			Queue<Integer> pizza,
			Semaphore mutex,
			Semaphore pizzaEmpty,
			Semaphore pizzaFull,
			Semaphore pizzaRequest)
	{
		this.pizza = pizza;
		this.mutex = mutex;
		this.pizzaFull = pizzaFull;
		this.pizzaEmpty = pizzaEmpty;
		this.pizzaRequest = pizzaRequest;
	}

	@Override
	public void run()
	{
		while (true) {
			try {
				pizzaRequest.acquire();
				mutex.acquire();
				for (int i = 0; i < Main.PIZZA_PIECES; i++) {
					pizza.add(i);
					pizzaEmpty.acquire();
					pizzaFull.release();
				}
				String message = "O entregador de pizza %s entregou uma nova pizza.";
				System.out.println(String.format(message, this));
				mutex.release();
				Thread.sleep((1 + (int) (Math.random() * (5 - 1 + 1))) * 1000);
			} catch (Exception e) {
				String message = "Uma excecao foi lancada enquanto o entregador de pizza %s tentava entregar uma nova pizza.";
				System.err.println(String.format(message, this));
				System.exit(1);
			}
		}
	}

}
