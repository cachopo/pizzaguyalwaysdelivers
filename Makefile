compile:
	javac Main.java PizzaGuy.java Student.java

package: compile
	jar -cfe PizzaGuyAlwaysDelivers.jar Main Main.class PizzaGuy.class Student.class

exec: package
	java -jar PizzaGuyAlwaysDelivers.jar

clean:
	$(RM) Main.class PizzaGuy.class PizzaGuyAlwaysDelivers.jar Student.class
