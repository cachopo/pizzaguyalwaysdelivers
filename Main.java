import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Semaphore;

public class Main {

	public static final int PIZZA_GUYS = 2;

	public static final int PIZZA_PIECES = 8;

	public static final int STUDENTS = 4;

	public static void main(String args[])
	{
		Queue<Integer> pizza = new LinkedList<Integer>();

		Semaphore mutex = new Semaphore(1);
		Semaphore pizzaRequest = new Semaphore(1);
		Semaphore pizzaEmpty = new Semaphore(Main.PIZZA_PIECES);
		Semaphore pizzaFull = new Semaphore(0);

		Thread[] pizzaGuys = new Thread[Main.PIZZA_GUYS];
		Thread[] students = new Thread[Main.STUDENTS];

		for (int i = 0; i < Main.PIZZA_GUYS; i++) {
			pizzaGuys[i] = new Thread(new PizzaGuy(
					pizza, mutex, pizzaEmpty, pizzaFull, pizzaRequest));
		}
		for (int i = 0; i < Main.STUDENTS; i++) {
			students[i] = new Thread(new Student(
					pizza, mutex, pizzaEmpty, pizzaFull, pizzaRequest));
		}

		for (int i = 0; i < Main.PIZZA_GUYS; i++) {
			pizzaGuys[i].start();
		}
		for (int i = 0; i < Main.STUDENTS; i++) {
			students[i].start();
		}

		try {
			for (int i = 0; i < Main.PIZZA_GUYS; i++) {
				pizzaGuys[i].join();
			}
			for (int i = 0; i < Main.STUDENTS; i++) {
				students[i].join();
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

}
