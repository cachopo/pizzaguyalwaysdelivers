import java.util.Queue;
import java.util.concurrent.Semaphore;

public class Student implements Runnable {

	private final Semaphore mutex;

	private final Queue<Integer> pizza;

	private final Semaphore pizzaEmpty;

	private final Semaphore pizzaFull;

	private final Semaphore pizzaRequest;

	public Student(
			Queue<Integer> pizza,
			Semaphore mutex,
			Semaphore pizzaEmpty,
			Semaphore pizzaFull,
			Semaphore pizzaRequest)
	{
		this.pizza = pizza;
		this.mutex = mutex;
		this.pizzaFull = pizzaFull;
		this.pizzaEmpty = pizzaEmpty;
		this.pizzaRequest = pizzaRequest;
	}

	@Override
	public void run()
	{
		while (true) {
			try {
				pizzaFull.acquire();
				mutex.acquire();
				if (pizza.size() == 1) {
					pizzaRequest.release();
				}
				String message = "%s comeu o %so. pedaco de pizza.";
				System.out.println(String.format(message, this, pizza.remove() + 1));
				mutex.release();
				pizzaEmpty.release();
				Thread.sleep((1 + (int) (Math.random() * (5 - 1 + 1))) * 1000);
			} catch (Exception e) {
				String message = "Uma excecao foi lancada enquanto o estudante %s tentava comer um pedaco de pizza.";
				System.err.println(String.format(message, this));
				System.exit(1);
			}
		}
	}

}
